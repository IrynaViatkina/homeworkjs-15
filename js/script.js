let timerId;
let start = document.getElementById('start');
let clear = document.getElementById('clear');

document.addEventListener('click', function(event){
	if(event.target.id == 'start'){
		start.textContent = 'Pause';
		start.id = 'pause';
		runTimer();
	}
	else if(event.target.id == 'clear'){
		stopTimer();
		document.getElementById('min').textContent = '00';
		document.getElementById('sec').textContent = '00';
		document.getElementById('millisec').textContent = '00';
	}
	else if(event.target.id == 'pause'){
		stopTimer();		
	}
});

function runTimer(){
	let millisec = document.getElementById('millisec').textContent;
	let sec = document.getElementById('sec').textContent;
	let min = document.getElementById('min').textContent;
	timerId = setInterval(function(){
		millisec = addIime('millisec');
		if(millisec == '99'){
			document.getElementById('millisec').textContent = '00';
			sec = addIime('sec');
			if(sec == '59'){
				document.getElementById('sec').textContent = '00';
				min = addIime('min');
			}
		}
	}, 10);
}

function stopTimer(){
	setTimeout(function() {
		clearInterval(timerId);
		start.textContent = 'Start';
		start.id = 'start';
	});
}
function addIime(id){
	let value = document.getElementById(id).textContent;
	if(value < 9){
		document.getElementById(id).textContent = `0${++value}`;
	}
	else{
		++document.getElementById(id).textContent;
	}
	return value;
}